package com.example;

public class AddOperation implements CalculatorOperation{

    @Override
    public double calculate(double first, double second) {
        return first + second;
    }
}
