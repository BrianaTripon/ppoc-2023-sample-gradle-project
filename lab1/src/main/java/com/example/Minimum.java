package com.example;

public class Minimum implements CalculatorOperation{

    @Override
    public double calculate(double first, double second) {
        return Math.min(first, second);
    }
}
