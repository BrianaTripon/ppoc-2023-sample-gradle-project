package com.example;

public class Maximum implements CalculatorOperation{

    @Override
    public double calculate(double first, double second) {
        return Math.max(first, second);
    }
}
