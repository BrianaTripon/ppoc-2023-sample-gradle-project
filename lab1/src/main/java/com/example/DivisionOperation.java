package com.example;

public class DivisionOperation implements CalculatorOperation{
    @Override
    public double calculate(double first, double second) {
        if(second == 0)
        {
            throw new ArithmeticException("Division by zero!");
        }
        return first/second;
    }
}
