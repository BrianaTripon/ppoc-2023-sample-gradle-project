package com.example;

public class MultiplyOperation implements CalculatorOperation{

    @Override
    public double calculate(double first, double second) {
        return first * second;
    }
}
