package com.example;

public class SubtractOperation implements CalculatorOperation{

    @Override
    public double calculate(double first, double second) {
        return first - second;
    }
}
