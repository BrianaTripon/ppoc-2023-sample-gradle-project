package com.example;

public class SquareRoot implements CalculatorOperation{

    @Override
    public double calculate(double first, double second) {
        return Math.sqrt(first);
    }
}
