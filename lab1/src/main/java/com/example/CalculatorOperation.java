package com.example;

public interface CalculatorOperation {
    double calculate(double first, double second);
}
