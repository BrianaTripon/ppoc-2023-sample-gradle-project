//package com.example;
//
//import org.checkerframework.checker.units.qual.C;
//
//import java.util.Scanner;
//
//public class Main {
//    private static CalculatorOperation getOperation(String operator) {
//        switch (operator) {
//            case "+":
//                return new AddOperation();
//            case "-":
//                return new SubtractOperation();
//            case "*":
//                return new MultiplyOperation();
//            case "/":
//                return new DivisionOperation();
//            case "min":
//                return new Minimum();
//            case "max":
//                return new Maximum();
//            case "sqrt":
//                return new SquareRoot();
//            default:
//                throw new IllegalArgumentException("Operator does not exist!");
//        }
//    }
//
//    public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);
//        Calculator calculator = new Calculator();
//
//        while(true) {
//            System.out.println("Enter an expression or exit to quit!");
//            String input = scanner.nextLine();
//
//            if(input.equals("exit")){
//                System.out.println("Goodbye!");
//                break;
//            }
//
//            try {
//                String[] tokens = input.split(" ");
//                double first = Integer.parseInt(tokens[0]);
//                String operator = tokens[1];
//                double second = Integer.parseInt(tokens[2]);
//
//                CalculatorOperation operation = getOperation(operator);
//                calculator.setOperation(operation);
//                double result = calculator.calculate(first, second);
//                System.out.println("Result: " + result);
//            } catch (Exception e) {
//                System.out.println("Error: " + e.getMessage());
//            }
//        }
//        scanner.close();
//    }
//}

package com.example;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");
    }
}
