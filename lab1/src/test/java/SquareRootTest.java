import com.example.CalculatorOperation;
import com.example.SquareRoot;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SquareRootTest {
    public CalculatorOperation operation;

    @BeforeEach
    public void setup(){
        operation = new SquareRoot();
    }

    @Test
    public void testAddWithPositiveNumbers() {
        double result = operation.calculate(16, 1);
        assertEquals(4, result);
    }
}
