import com.example.CalculatorOperation;
import com.example.Minimum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MinimumTest {
    public CalculatorOperation operation;

    @BeforeEach
    public void setup(){
        operation = new Minimum();
    }

    @Test
    public void testMinOfPositiveNumbers() {
        double result = operation.calculate(2, 3);
        assertEquals(2, result);
    }

}
