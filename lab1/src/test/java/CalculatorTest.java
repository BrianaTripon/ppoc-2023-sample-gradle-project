//import com.example.*;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//
//import org.junit.Test;
//
//public class CalculatorTest {
//
//    @Test
//    public void testSetOperation() {
//        Calculator calculator = new Calculator();
//        CalculatorOperation addition = new AddOperation();
//        calculator.setOperation(addition);
//        assertEquals(addition, calculator.getOperation());
//    }
//
//    @Test
//    public void testGetOperation() {
//        Calculator calculator = new Calculator();
//        CalculatorOperation addition = new AddOperation();
//        calculator.setOperation(addition);
//        CalculatorOperation retrievedOperation = calculator.getOperation();
//        assertEquals(addition, retrievedOperation);
//    }
//
//    @Test
//    public void testCalculateAddition() {
//        Calculator calculator = new Calculator();
//        CalculatorOperation addition = new AddOperation();
//        calculator.setOperation(addition);
//        double result = calculator.calculate(5, 3);
//        assertEquals(8, result);
//    }
//
//    @Test
//    public void testCalculateSubtraction() {
//        Calculator calculator = new Calculator();
//        CalculatorOperation subtraction = new SubtractOperation();
//        calculator.setOperation(subtraction);
//        double result = calculator.calculate(10, 3);
//        assertEquals(7, result);
//    }
//
//    @Test
//    public void testCalculateMultiplication() {
//        Calculator calculator = new Calculator();
//        CalculatorOperation multiplication = new MultiplyOperation();
//        calculator.setOperation(multiplication);
//        double result = calculator.calculate(4, 5);
//        assertEquals(20, result);
//    }
//
//    @Test
//    public void testCalculateDivision() {
//        Calculator calculator = new Calculator();
//        CalculatorOperation division = new DivisionOperation();
//        calculator.setOperation(division);
//        double result = calculator.calculate(15, 3);
//        assertEquals(5, result);
//    }
//
//    @Test
//    public void testCalculateMinimum() {
//        Calculator calculator = new Calculator();
//        CalculatorOperation minimum = new Minimum();
//        calculator.setOperation(minimum);
//        double result = calculator.calculate(7, 4);
//        assertEquals(4, result);
//    }
//
//    @Test
//    public void testCalculateMaximum() {
//        Calculator calculator = new Calculator();
//        CalculatorOperation maximum = new Maximum();
//        calculator.setOperation(maximum);
//        double result = calculator.calculate(7, 4);
//        assertEquals(7, result);
//    }
//
//    @Test
//    public void testCalculateSquareRoot() {
//        Calculator calculator = new Calculator();
//        CalculatorOperation squareRoot = new SquareRoot();
//        calculator.setOperation(squareRoot);
//        double result = calculator.calculate(16, 0);
//        assertEquals(4, result);
//    }
//
////    @Test
////    public void testSetAdditionOperation() {
////        Calculator calculator = new Calculator();
////        CalculatorOperation addition = new AddOperation();
////        calculator.setOperation(addition);
////        assertEquals(addition, calculator.getOperation());
////    }
////
////    @Test
////    public void TestCalculatorAddOperation() {
////        Calculator calculator = new Calculator();
////        CalculatorOperation operation = new AddOperation();
////        calculator.setOperation(operation);
////
////        double result = calculator.calculate(2, 3);
////        assertEquals(5, result);
////    }
//
//
//}
