//import com.example.CalculatorOperation;
//import com.example.DivisionOperation;
//import org.junit.Test;
//import org.junit.jupiter.api.BeforeEach;
//
//import static org.junit.Assert.assertEquals;
import com.example.AddOperation;
import com.example.CalculatorOperation;
import com.example.DivisionOperation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class DivisionOperationTest {
    public CalculatorOperation operation;

    @BeforeEach
    public void setup() {
        operation = new DivisionOperation();
    }

    @Test
    public void testDivisionOfPositiveNumbers() {
        double result = operation.calculate(4, 2);
        assertEquals(2, result);
    }

    @Test
    public void testDivisionOfNegativeNumbers() {
        double result = operation.calculate(-4, -2);
        assertEquals(2, result);
    }

    @Test
    public void testDivisionOfPositiveWithNegativeNumber() {
        double result = operation.calculate(4, -2);
        assertEquals(-2, result);
    }

    @Test
    public void testDivisionOfNegativeWithPositiveNumber() {
        double result = operation.calculate(-4, 2);
        assertEquals(-2, result);
    }

    @Test
    public void testDivisionWithZeroAsNumerator() {
        double result = operation.calculate(0, 4);
        assertEquals(0, result);
    }

    @Test
    public void testCalculateWithZeroAsDivisor() {
        CalculatorOperation division = new DivisionOperation();
        try {
            double result = division.calculate(5, 0);
            fail("Expected ArithmeticException was not thrown.");
        } catch (ArithmeticException e) {
        }
    }

//    @Test(expected = ArithmeticException.class)
//    public void testDivisionByZero() {
//        operation.calculate(6, 0);
//    }
}

