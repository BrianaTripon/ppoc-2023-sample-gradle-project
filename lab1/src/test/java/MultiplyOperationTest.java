import com.example.CalculatorOperation;
import com.example.MultiplyOperation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MultiplyOperationTest {
    public CalculatorOperation operation;

    @BeforeEach
    public void setup() {
        operation = new MultiplyOperation();
    }

    @Test
    public void testMultiplicationOfPositiveNumbers() {
        double result = operation.calculate(3, 4);
        assertEquals(12, result);
    }

    @Test
    public void testMultiplicationOfNegativeNumbers() {
        double result = operation.calculate(-3, -4);
        assertEquals(12, result);
    }

    @Test
    public void testMultiplicationOfPositiveWithNegativeNumber() {
        double result = operation.calculate(3, -4);
        assertEquals(-12, result);
    }

    @Test
    public void testMultiplicationWithZero() {
        double result = operation.calculate(3, 0);
        assertEquals(0, result);
    }
}
