import com.example.CalculatorOperation;
import com.example.Maximum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MaximumTest {
    public CalculatorOperation operation;

    @BeforeEach
    public void setup(){
        operation = new Maximum();
    }

    @Test
    public void testMaxOfPositiveNumbers() {
        double result = operation.calculate(2, 3);
        assertEquals(3, result);
    }
}
