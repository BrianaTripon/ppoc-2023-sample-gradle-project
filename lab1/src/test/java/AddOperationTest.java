import com.example.AddOperation;
import com.example.CalculatorOperation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class AddOperationTest {
    public CalculatorOperation operation;

    @BeforeEach
    public void setup(){
        operation = new AddOperation();
    }

    @Test
    public void testAddWithPositiveNumbers() {
        double result = operation.calculate(2, 3);
        assertEquals(5, result);
    }

    @Test
    public void testAddWithNegativeNumbers() {
        double result = operation.calculate(-1, -1);
        assertEquals(-2, result);
    }

    @Test
    public void testAddPositiveAndNegativeNumbers() {
        double result = operation.calculate(3, -1);
        assertEquals(2, result);
    }

    @Test
    public void testAddWithZero() {
        double result = operation.calculate(0, 2);
        assertEquals(2, result);
    }
}
