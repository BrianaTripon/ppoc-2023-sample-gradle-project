import com.example.CalculatorOperation;
import com.example.SubtractOperation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SubtractOperationTest {
    public CalculatorOperation operation;

    @BeforeEach
    public void setup() {
        operation = new SubtractOperation();
    }

    @Test
    public void testSubtractionWithPositiveNumbers() {
        double result = operation.calculate(6, 3);
        assertEquals(3, result);
    }

    @Test
    public void testSubtractionWithNegativeNumbers() {
        double result = operation.calculate(-2, -3);
        assertEquals(1, result);
    }

    @Test
    public void testSubtractionWithNegativeResult() {
        double result = operation.calculate(3, 4);
        assertEquals(-1, result);
    }

    @Test
    public void testSubtractionWithZero() {
        double result = operation.calculate(5, 0);
        assertEquals(5, result);
    }
}
